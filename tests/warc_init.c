/*
 * Copyright (C) 2020 Free Software Foundation, Inc.
 *
 * This file is part of libWARC.
 *
 * libwarc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libwarc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libwarc.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <config.h>

#include <stdio.h>
#include <stdlib.h>
#include <check.h>
#include <string.h>

#include "libwarc.h"
#include "private.h"
#include "file.h"
#include "digest.h"
#include "private.h"
#include "cdx.h"
#include "hashmap.h"

/*
 * This file contains tests for the LibWARC initialization
 * procedures.
 *
 * The initialization of LibWARC includes the following procedures:
 * 1. Loading CDX records into a table for deduplication.
 * 2. Opening temporary files for manifest, log and config data.
 * 3. Starting a CDX file for the current WARC session.
 * 4. Starting a new WARC file for the current WARC session.
 *
 * Each of these procedures have their own tests and test cases in this
 * file.
 *
 * The initialization also depends on the temporary file creation utility
 * carried out through the function `create_temp_file` which is also
 * tested in this file.
 */

/*
 * This test is for testing the function `create_temp_file`.
 *
 * The procedure here is:
 * 1. A temporary file is created using `create_temp_file`.
 * 2. Some known data is written to it.
 * 3. The file is then read from and compared to the original
 * data.
 * 4. If the read and original data are the same, the test passes.
 * 5. If the read and original data are not the same, the test
 * fails.
 */
START_TEST(test_temp_file)
{
    int flag = 1;

    // Supplying NULL essentially means that the temp files are
    // created in the default dirrectory `/tmp/libwarc/`
    FILE *f = create_temp_file(NULL);
    if (f == NULL) {
        flag = 0;
    }

    char str[] = "This is a temp file";
    char buf[strlen(str) + 1];

    fwrite(str, 1, strlen(str) + 1, f);
    fseek(f, 0, SEEK_SET);
    size_t status = fread(buf, 1, strlen(str) + 1, f);

    if (status == 0) {
        perror("Read unsuccessful");
        flag = 0;
    }
    if (STRNEQ(buf, str)) {
        perror("STRNEQ unsuccessful");
        flag = 0;
    }

    if (f != NULL) {
        fclose(f);
    }

    ck_assert_int_eq(flag, 1);
} END_TEST

/*
 * This test is for testing the new WARC file creation
 * procedure with the function `start_warc_file`.
 *
 * The procedure here is:
 * 1. Allocate memory for `warc_session` and `warc_options`.
 * 2. Call `warc_init` for creating the new WARC file.
 * 3. Write some known data to the created WARC file.
 * 4. Read the data from the file and compare it the
 * original data.
 * 5. If the read and original data are the same, the test
 * passes.
 * 6. If the read and the original data are not the same, the
 * test fails.
 *
 * This test will be further expanded when the Warcinfo record
 * writing functionality is introduced.
 */
START_TEST(test_start_warc_file)
{
    int flag = 1;

    // Initialize LibWARC
    warc_options *options = malloc(sizeof(warc_options));
    warc_session *session;
    warc_get_default_options(options);
    options->warc_filename = strdup("session1");
    if ((session = warc_init(options)) == NULL) {
        flag = 0;
    }

    // Data to be written in the WARC file
    char str[] = "This is a warc file";
    char *buf = malloc((strlen(str) + 1) * sizeof(char));

    // Write the data
    fwrite(str, 1, strlen(str), session->current_file);
    fseek(session->current_file, 0, SEEK_SET);

    // Read the written data
    buf = fgets(buf, ((int)strlen(str)) + 1, session->current_file);

    // Compare the read data with the original
    if (STRNEQ(buf, str)) {
        perror("STRNEQ unsuccessful");
        flag = 0;
    }

    free(options->warc_filename);
    free(options);
    warc_close(session);
    free(buf);
    if (system("rm session1-*.warc") == -1) {
        flag = 0;
    }

    ck_assert_int_eq(flag, 1);
} END_TEST

/*
 * This is for testing the procedure to load CDX records into a hashmap for
 * deduplication. This procedure includes four functions:
 * 1. write_cdx_records_dedup
 * 2. count_cdx_records
 * 3. parse_cdx_header
 * 4. process_cdx_line
 *
 * The testing procedure is:
 * 1. Write some CDX records into a file. This will be the CDX dedup file.
 * 2. This file is then supplied to the `write_cdx_records_dedup` function.
 * 3. Since the deduplication table is a hashmap, we use the hashmap functions
 * to check all its entries.
 * 4. We use the known digests as key and access their corresponding values in the
 * table.
 * 5. If the values in the hashmap are the same as the original values, the test
 * passes.
 * 6. If the values in the hashmap are not the same as the original values, the test
 * fails.
 *
 * In this way, it tests whether CDX records are correctly loaded into a hashmap using
 * the above functions.
 */
START_TEST(test_cdx_dedup)
{
    int flag = 1;

    FILE *f = fopen("samplefile.cdx", "w+");
    if (f == NULL) {
        flag = 0;
    }

    // Setting the memory management function
    warc_malloc = malloc;
    warc_free = free;

    // The content to be written into the CDX file
    char str[] = "CDX a b m s k g\n"
                 "SampleURL1 2020-07-07T08:26:47Z text/html "
                 "200 S7QVBAIAXN77VLMILDZVAHP3L23U7H22 samplefile1.warc\n"
                 "SampleURL2 2020-07-07T08:26:47Z text/html "
                 "200 7OH4CFA5HJC6MYRHBMFQJAX2PSSTR27M samplefile2.warc\n"
                 "SampleURL3 2020-07-07T08:26:47Z text/html "
                 "200 BHQWKATI25NOB6UCVU67JWLPHA2XBK7U samplefile3.warc\n"
                 "SampleURL4 2020-07-07T08:26:47Z text/html "
                 "200 XN6CB2FGZGYB5CMOQVCQM4NKGH4XR7QJ samplefile4.warc\n";
    fwrite(str, 1, strlen(str), f);
    fclose(f);

    // Allocating space `warc_session`
    warc_session *session = malloc(sizeof(warc_session));

    // Calling `write_cdx_records_dedup`
    FILE *cdx = fopen("samplefile.cdx", "r+");
    int status = write_cdx_records_dedup(cdx, session);
    if (status == -1) {
        flag = 0;
    }

    // Create the key, date and the URL to test with
    char digest[][33] = {"S7QVBAIAXN77VLMILDZVAHP3L23U7H22",
                         "7OH4CFA5HJC6MYRHBMFQJAX2PSSTR27M",
                         "BHQWKATI25NOB6UCVU67JWLPHA2XBK7U",
                         "XN6CB2FGZGYB5CMOQVCQM4NKGH4XR7QJ"};
    char date[][22] = {"2020-07-07T08:26:47Z",
                       "2020-07-07T08:26:47Z",
                       "2020-07-07T08:26:47Z",
                       "2020-07-07T08:26:47Z"};
    char url[][11] = {"SampleURL1",
                      "SampleURL2",
                      "SampleURL3",
                      "SampleURL4"};
    char *key = malloc((SHA1_DIGEST_SIZE) * sizeof(char));

    // Check values in the hash table
    for (int i = 0; i < 4; i++) {
        decode_base32_digest(digest[i], key);
        cdx_record *value = NULL;

        // Call `hashmap_get` to obtain the value for the key
        if (hashmap_get(session->dedup_table, key, &value) == 0) {
            perror("Record not found");
        }

        // Check if the obtained value is correct
        if (!(value != NULL && STREQ(value->url, url[i]) &&
              compare_digest(key, value->digest) == 0 &&
              STREQ(value->date, date[i]))) {
            flag = 0;
            break;
        }
    }

    free_dedup_table(session);
    free(session);

    remove("samplefile.cdx");

    ck_assert_int_eq(flag, 1);
} END_TEST

/*
 * This is for testing the function `start_cdx_file`.
 *
 * The procedure here is:
 * 1. Allocate memory for the session and options structs.
 * 2. Call the function `start_cdx_file`.
 * 3. Read data from the CDX file created.
 * 4. If the data is the same as the expected CDX header `correct_line`,
 * then the test passes.
 * 5. If the data is not the same as the expected CDX header,
 * the test fails.
 */
START_TEST(test_start_cdx_file)
{
    int flag = 1;

    // Expected CDX header
    char correct_line[] = "CDX a b m s k g\n";

    // Allocate memory for the required structs
    warc_session *session = malloc(sizeof(warc_session));
    warc_options *options = malloc(sizeof(warc_options));
    warc_get_default_options(options);
    options->warc_filename = strdup("session1");

    session->options = options;

    // Call `start_cdx_file`
    if (start_cdx_file(session) == -1) {
        flag = 0;
    }
    if (session->cdx_file == NULL) {
        flag = 0;
    }

    // Seek to the start of the CDX file
    if (fseek(session->cdx_file, 0, SEEK_SET) != 0) {
        flag = 0;
    }

    // Compare the written CDX header to the expected header
    char *line = NULL;
    size_t n = 0;
    ssize_t line_length = getline(&line, &n, session->cdx_file);

    if (line_length == -1) {
        flag = 0;
    }
    if (STRNEQ(line, correct_line)) {
        flag = 0;
    }

    free(line);
    free(options->warc_filename);
    free(options);
    free(session);
    remove("session1.cdx");

    ck_assert_int_eq(flag, 1);
} END_TEST

/*
 * This test suite is for testing functions related to the
 * initialization of libWARC.
 */
static Suite *make_warc_init_suite(void)
{
    Suite *s;
    TCase *tc_temp_file, *tc_start_warc_file, *tc_cdx_dedup, *tc_start_cdx_file;

    s = suite_create("warc_init");

    tc_temp_file = tcase_create("temp_file");
    tc_start_warc_file = tcase_create("start_warc_file");
    tc_cdx_dedup = tcase_create("cdx_dedup");
    tc_start_cdx_file = tcase_create("start_cdx_file");

    tcase_add_test(tc_temp_file, test_temp_file);
    tcase_add_test(tc_start_warc_file, test_start_warc_file);
    tcase_add_test(tc_cdx_dedup, test_cdx_dedup);
    tcase_add_test(tc_start_cdx_file, test_start_cdx_file);

    suite_add_tcase(s, tc_temp_file);
    suite_add_tcase(s, tc_start_warc_file);
    suite_add_tcase(s, tc_cdx_dedup);
    suite_add_tcase(s, tc_start_cdx_file);

    return s;
}

int main(void)
{
    int no_failed = 0;
    SRunner *runner;

    runner = srunner_create(make_warc_init_suite());

    srunner_set_tap(runner, "libwarc-warc-init.tap");
    srunner_run_all(runner, CK_NORMAL);
    no_failed = srunner_ntests_failed(runner);
    srunner_free(runner);
    return (no_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
