/*
 * Copyright (C) 2020 Free Software Foundation, Inc.
 *
 * This file is part of libWARC.
 *
 * libwarc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libwarc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libwarc.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <config.h>
#include <stdio.h>

#include <stdlib.h>
#include <sha1.h>
#include <base32.h>
#include <string.h>

#include "libwarc.h"
#include "cdx.h"
#include "digest.h"
#include "hashmap.h"
#include "private.h"

/**
 * \file
 * \brief Functions for performing operations on CDX index files
 */

/**
 * \brief The delimiter string for the tokenization of the CDX header and record lines
 */
#define CDX_FIELD_DELIM " \n\r\t"

/**
 * \param[in] cdx_file The path to the CDX file
 *
 * \return The number of records in the CDX file
 *
 * \brief Count the number of records in the given CDX file
 *
 * This function is to count the number of CDX records in the CDX dedup file.
 * It returns 0 in case of an error or when the number of records is zero.
 */
static size_t count_cdx_records(FILE *cdx_file)
{
    size_t number_of_records = 0;

    // Seek to the start of the CDX file
    if (fseeko(cdx_file, 0, SEEK_SET) != 0) {
        // Return value of 0 indicates error as well
        return 0;
    }

    // Count the number of lines
    while (!feof(cdx_file)) {
        int ch = fgetc(cdx_file);
        if (ch == '\n') {
            number_of_records++;
        }
    }

    // Seek to the start of the CDX file
    if (fseeko(cdx_file, 0, SEEK_SET) != 0) {
        // Return value of 0 indicates error as well
        return 0;
    }

    // To account for the CDX header
    number_of_records--;

    return number_of_records;
}

/**
 * \param[in] line The CDX header line
 * \param[out] original_url_num The pointer to the position of the original url field
 * \param[out] checksum_num The pointer to the position of the checksum field
 * \param[out] date_num The pointer to the position of the date field
 *
 * \brief Parse the CDX header
 *
 * The procedure here is essentially to tokenize the CDX header string and check the position
 * of the original URL, the checksum(digest) and the date fields.
 */
static void parse_cdx_header(char *line, int *original_url_num, int *checksum_num, int *date_num)
{
    char *token, *save_ptr;

    // Initialize the field positions to -1, indicating 'not found'
    *original_url_num = -1;
    *date_num = -1;
    *checksum_num = -1;

    token = strtok_r(line, CDX_FIELD_DELIM, &save_ptr);
    int current_field = 0;
    /*
     * Read each character in the header and the set the positions for
     * the required fields
     */
    while (token != NULL) {
        switch (token[0]) {
        case 'a':
            *original_url_num = current_field - 1;
            break;
        case 'b':
            *date_num = current_field - 1;
            break;
        case 'k':
            *checksum_num = current_field - 1;
            break;
        default:
            break;
        }
        current_field++;
        token = strtok_r(NULL, CDX_FIELD_DELIM, &save_ptr);
    }
}

/**
 * \param[in] line The current CDX line to be processed
 * \param[in] original_url_position The position of the original URL field in the current line
 * \param[in] date_position The position of the date field in the current line
 * \param[in] checksum_position The position of the checksum(digest) field in the current line
 * \param[in] session The context struct for the current WARC session
 *
 * \return 0 If the procedure is successful
 * \return -1 If the procedure is not successful
 *
 * \brief Process the current CDX record line and add an entry to the Hashmap
 *
 * The procedure here is that the line it receives is tokenized and the useful data like the original
 * URL, date and checksum is extracted. This data is then added to a record struct which is then given as
 * value to an entry in the Hashmap.
 */
static int process_cdx_line(char *line, const int original_url_position, const int date_position,
                            const int checksum_position, const warc_session *session)
{
    char *original_url = NULL;
    // Here, 22 is the length of the UTC timestamp described in ISO 8601
    char date[22];
    // Here, the actual length of the checksum is the Digest header with the `sha1:`
    // label removed
    char checksum[DIGEST_HEADER_SIZE - 5];
    char *token;
    char *save_ptr;
    int current_field = 0;

    // Tokenize the record and extract the URL, date and checksum(digest)
    token = strtok_r(line, CDX_FIELD_DELIM, &save_ptr);
    while (token != NULL) {
        if (current_field == original_url_position) {
            original_url = warc_malloc(strlen(token) + 1);
            strcpy(original_url, token);
        }
        if (current_field == date_position) {
            strcpy(date, token);
        }
        if (current_field == checksum_position) {
            strcpy(checksum, token);
        }
        current_field++;
        token = strtok_r(NULL, CDX_FIELD_DELIM, &save_ptr);
    }

    /*
     * Construct the value struct for the record containing the URL, date and digest.
     * Here, we are taking the binary version of the digest to make the digest
     * smaller in size and to make the hashing for the hashmap easier
     */
    if (original_url != NULL || date != NULL || checksum != NULL) {
        // Allocate memory for a new CDX record
        cdx_record *new_record = warc_malloc(sizeof(cdx_record));

        // Allocate memory for the URL member
        new_record->url = warc_malloc(strlen(original_url) + 1);

        // Copy URL and Date into the CDX record struct
        strcpy(new_record->url, original_url);
        strcpy(new_record->date, date);

        // Since we don't need response ID for CDX deduplication, set it to NULL
        new_record->response_id = NULL;

        // Decode the given base32 digest to binary
        new_record->digest = warc_malloc(SHA1_DIGEST_SIZE);
        if (decode_base32_digest(checksum, new_record->digest) == -1) {
            warc_free(original_url);
            warc_free(new_record->url);
            warc_free(new_record->digest);
            warc_free(new_record);
            return -1;
        }

        // Add an entry into the hashmap
        int hashmap_status = hashmap_put(session->dedup_table, new_record->digest, new_record);
        if (hashmap_status == -1) {
            warc_free(original_url);
            warc_free(new_record->url);
            warc_free(new_record->digest);
            warc_free(new_record);
            return -1;
        }
        warc_free(original_url);
    } else {
        warc_free(original_url);
        return -1;
    }

    return 0;
}

/**
 * \param[in] cdx_dedup_file The CDX dedup file pointer
 * \param[in] session The context struct for the current WARC session
 *
 * \return 0 If the creation of the hashmap is successful
 * \return -1 If the creation of the hashmap is unsuccessful
 *
 * \brief Write the CDX dedup records to a hashmap
 *
 * Procedure:
 * 1. Parse the first line of the CDX file using `parse_cdx_header`.
 * 2. Create a hashmap using `hashmap_create`.
 * 3. Add each line to the Hashmap using `process_cdx_line`.
 */
int write_cdx_records_dedup(FILE *cdx_dedup_file, warc_session *session)
{
    size_t record_num = count_cdx_records(cdx_dedup_file);
    if (record_num <= 0) {
        return -1;
    }

    // Extract the first line
    size_t n = 0;
    char *line = NULL;
    ssize_t line_length = getline(&line, &n, cdx_dedup_file);
    if (line_length == -1) {
        warc_free(line);
        fclose(cdx_dedup_file);
        return -1;
    }

    /*
     * Parse the CDX header. This would find the positions of the essential
     * CDX fields for use during dedup, to find duplicate records.
     */
    int original_url_position, checksum_position, date_position;
    parse_cdx_header(line, &original_url_position, &checksum_position, &date_position);
    if (original_url_position == -1 || checksum_position == -1 || date_position == -1) {
        warc_free(line);
        fclose(cdx_dedup_file);
        return -1;
    }

    /*
     * Create the Hashmap. Here, the max size is essentially the number of records in the
     * CDX file
     */
    int max_hashmap_size = (int)record_num;
    session->dedup_table = hashmap_create(max_hashmap_size, hash_sha1_digest, compare_digest);

    if (session->dedup_table == NULL) {
        warc_free(line);
        free_dedup_table(session);
        fclose(cdx_dedup_file);
        return -1;
    }

    // Process each CDX record line
    do {
        line_length = getline(&line, &n, cdx_dedup_file);
        if (line_length != -1) {
            int status = process_cdx_line(line, original_url_position, date_position,
                                          checksum_position, session);
            if (status == -1) {
                warc_free(line);
                free_dedup_table(session);
                fclose(cdx_dedup_file);
                return -1;
            }
        } else if (!feof(cdx_dedup_file)) {
            warc_free(line);
            free_dedup_table(session);
            fclose(cdx_dedup_file);
            return -1;
        }
    } while (line_length != -1);

    warc_free(line);
    fclose(cdx_dedup_file);

    return 0;
}

/**
 * \param[in] session Context struct for the current WARC session
 *
 * \brief Free the current CDX deduplication table
 */
void free_dedup_table(warc_session *session)
{
    // Return if session->dedup_table is NULL
    if (session->dedup_table == NULL) {
        return;
    }

    // Create an iterator for the hashmap
    hashmap_iterator *i = hashmap_iterator_alloc(session->dedup_table);
    void *value = NULL;

    // Free all individually allocated members in the hashmap
    while (hashmap_iterator_next(i, &value)) {
        warc_free(((cdx_record *)value)->url);
        warc_free(((cdx_record *)value)->response_id);
    }

    // Free iterator
    hashmap_iterator_free(&i);

    // Free hashmap
    hashmap_free(&(session->dedup_table));
    session->dedup_table = NULL;
}

/**
 * \param[in] session The context struct for the current WARC session
 *
 * \return 0 If the procedure was successful
 * \return -1 If the procedure was not successful
 *
 * \brief Start a new CDX file
 *
 * Procedure:
 * 1. Create a filename based on the base WARC filename given by the user.
 * 2. Open the file using the filename.
 * 3. Add the file pointer to the context struct \p session.
 * 4. Write the CDX header to the file.
 */
int start_cdx_file(warc_session *session)
{
    // Check if base filename is supplied by the user
    if (session->options->warc_filename == NULL) {
        return -1;
    }

    // Construct the CDX filename
    char cdx_filename[strlen(session->options->warc_filename) + strlen(".cdx") + 1];
    sprintf(cdx_filename, "%s.cdx", session->options->warc_filename);

    // Open the CDX file
    FILE *cdx_file = fopen(cdx_filename, "w+");

    if (cdx_file == NULL) {
        return -1;
    }

    session->cdx_file = cdx_file;

    /*
     * CDX header fields:
     * a - original url
     * b - date
     * m - mime type of the original document
     * s - response code
     * k - new style checksum
     * g - file name
     */
    fprintf(session->cdx_file, "CDX a b m s k g\n");
    fflush(session->cdx_file);

    return 0;
}
