/*
 * Copyright (C) 2020 Free Software Foundation, Inc.
 *
 * This file is part of libWARC.
 *
 * libwarc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libwarc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libwarc.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef LIBWARC_DIGEST_H
#define LIBWARC_DIGEST_H

#include <config.h>

#include "attribute.h"

/**
 * \file
 *
 * \brief The header file for digest calculation functions
 */

/**
 * \brief The size for a string containing a digest header
 */
#define DIGEST_HEADER_SIZE 38

ATTRIBUTE_PURE int compare_digest(const void *digest1, const void *digest2);
ATTRIBUTE_PURE unsigned int hash_sha1_digest(const void *key);
int get_digest_headers(FILE *f, const long payload_offset, char *block, char *payload);
int decode_base32_digest(const char *b32_digest, char *bin_digest);

#endif /* LIBWARC_DIGEST_H */
