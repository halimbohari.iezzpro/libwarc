/*
 * Copyright (C) 2020 Free Software Foundation, Inc.
 *
 * This file is part of libWARC.
 *
 * libwarc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libwarc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libwarc.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <config.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

#include "libwarc.h"
#include "hashmap.h"

/**
 * \file
 * \brief Hashmap functions
 *
 * Hashmaps are key/value stores that perform at O(1) for insertion, searching and removing.
 *
 * This particular file and the corresponding header file `hashmap.h` were taken from libwget
 */

/**
 * \brief Typedef for entry_st
 */
typedef struct entry_st entry_t;

/**
 * \brief The struct representing an entry object
 */
struct entry_st {
    void *key; /**< Pointer to key */
    void *value; /**< Pointer to value */
    entry_t *next; /**< Pointer to the next element in the list */
    unsigned int hash; /**< The hash of the key */
};

/**
 * \brief The struct representing a hashmap
 */
struct hashmap_st {
    hashmap_hash_fn *hash; /**< hash function */
    hashmap_compare_fn *cmp; /**< compare function */
    hashmap_key_destructor *key_destructor; /**< key destructor function */
    hashmap_value_destructor *value_destructor; /**< value destructor function */
    entry_t **entry; /**< pointer to array of pointers to entries */
    int max; /**< allocated entries */
    int cur; /**< number of entries in use */
    int threshold; /**< resize when max reaches threshold */
    float resize_factor; /**< resize strategy: >0: resize = off * max, <0: resize = max + (-off) */
    float load_factor; /**< Load Factor */
};

/**
 * \brief The struct representing a hashmap iterator
 */
struct hashmap_iterator_st {
    struct hashmap_st *h; /**< The hashmap struct to create the iterator of */
    entry_t *entry; /**< The pointer to the entry object */
    int pos; /**< The current position of the iterator */
};

/**
 * \param[in] h Hashmap
 * \return New iterator instance for \p h
 *
 * Creates a hashmap iterator for \p h.
 */
hashmap_iterator *hashmap_iterator_alloc(hashmap *h)
{
    struct hashmap_iterator_st *iter = calloc(1, sizeof(struct hashmap_iterator_st));

    if (iter) {
        iter->h = h;
    }

    return (hashmap_iterator *)iter;
}

/**
 * \param[in] iter Hashmap iterator
 *
 * Free the given iterator \p iter.
 */
void hashmap_iterator_free(hashmap_iterator **iter)
{
    if (iter) {
        warc_free(*iter);
    }
}

/**
 * \param[in] iter Hashmap iterator
 * \param[out] value Pointer to the value belonging to the returned key
 * \return Pointer to the key or NULL if no more elements left
 *
 * Returns the next key / value in the hashmap. If all key/value pairs have been
 * iterated over the function returns NULL and \p value is untouched.
 *
 * When iterating over a hashmap, the order of returned key/value pairs is not defined.
 */
void *hashmap_iterator_next(hashmap_iterator *iter, void **value)
{
    struct hashmap_iterator_st *_iter = (struct hashmap_iterator_st *)iter;
    struct hashmap_st *h = _iter->h;

    if (_iter->entry) {
        if ((_iter->entry = _iter->entry->next)) {
        found:
            if (value) {
                *value = _iter->entry->value;
            }
            return _iter->entry->key;
        }

        _iter->pos++;
    }

    if (!_iter->entry) {
        for (; _iter->pos < h->max; _iter->pos++) {
            if (h->entry[_iter->pos]) {
                _iter->entry = h->entry[_iter->pos];
                goto found;
            }
        }
    }

    return NULL;
}

/**
 * \param[in] max Initial number of pre-allocated entries
 * \param[in] hash Hash function to build hashes from elements
 * \param[in] cmp Comparison function used to find elements
 * \return New hashmap instance
 *
 * Create a new hashmap instance with initial size \p max.
 * It should be free'd after use with hashmap_free().
 *
 * Before the first insertion of an element, \p hash and \p cmp must be set.
 * So if you use %NULL values here, you have to call hashmap_setcmpfunc() and/or
 * hashmap_hashcmpfunc() with appropriate function pointers. No doing so will result
 * in undefined behavior (likely you'll see a segmentation fault).
 */
hashmap *hashmap_create(int max, hashmap_hash_fn *hash, hashmap_compare_fn *cmp)
{
    hashmap *h = warc_malloc(sizeof(hashmap));

    if (!h) {
        return NULL;
    }

    h->entry = calloc((size_t)max, sizeof(entry_t *));

    if (!h->entry) {
        warc_free(h);
        return NULL;
    }

    h->max = max;
    h->cur = 0;
    h->resize_factor = 2;
    h->hash = hash;
    h->cmp = cmp;
    h->key_destructor = warc_free;
    h->value_destructor = warc_free;
    h->load_factor = 0.75f;
    h->threshold = (int)((float)max * h->load_factor);

    return h;
}

static entry_t *hashmap_find_entry(const hashmap *h, const char *key, unsigned int hash)
{
    for (entry_t *e = h->entry[hash % (unsigned int)h->max]; e; e = e->next) {
        if (hash == e->hash && (key == e->key || !h->cmp(key, e->key))) {
            return e;
        }
    }

    return NULL;
}

static void hashmap_rehash(hashmap *h, entry_t **new_entry, int newmax, int recalc_hash)
{
    entry_t *entry, *next;
    int cur = h->cur;

    for (int it = 0; it < h->max && cur; it++) {
        for (entry = h->entry[it]; entry; entry = next) {
            next = entry->next;

            // now move entry from 'h' to 'new_hashmap'
            if (recalc_hash) {
                entry->hash = h->hash(entry->key);
            }
            int pos = (int)(entry->hash % (unsigned int)newmax);
            entry->next = new_entry[pos];
            new_entry[pos] = entry;

            cur--;
        }
    }

    warc_free(h->entry);
    h->entry = new_entry;
    h->max = newmax;
    h->threshold = (int)((float)newmax * h->load_factor);
}

static int hashmap_new_entry(hashmap *h, unsigned int hash, char *key, char *value)
{
    entry_t *entry;

    if (!(entry = warc_malloc(sizeof(entry_t)))) {
        return -1;
    }

    int pos = (int)(hash % (unsigned int)h->max);

    entry->key = (void *)key;
    entry->value = (void *)value;
    entry->hash = hash;
    entry->next = h->entry[pos];
    h->entry[pos] = entry;

    if (++h->cur >= h->threshold) {
        int newsize = (int)((float)h->max * h->resize_factor);

        if (newsize > 0) {
            entry_t **new_entry;

            if (!(new_entry = calloc((size_t)newsize, sizeof(entry_t *)))) {
                h->cur--;
                warc_free(h->entry[pos]);
                return -1;
            }

            // h->cur is always > 0 here, so we don't need a check
            hashmap_rehash(h, new_entry, newsize, 0);
        }
    }

    return 0;
}

/**
 * \param[in] h Hashmap to put data into
 * \param[in] key Key to insert into \p h
 * \param[in] value Value to insert into \p h
 * \return 0 if inserted a new entry, 1 if entry existed, -1 if internal allocation failed
 *
 * Insert a key/value pair into hashmap \p h.
 *
 * \p key and \p value are *not* cloned, the hashmap takes 'ownership' of both.
 *
 * If \p key already exists and the pointer values the old and the new key differ,
 * the old key will be destroyed by calling the key destructor function (default is warc_free()).
 *
 * To realize a hashset (just keys without values), \p value may be %NULL.
 *
 * Neither \p h nor \p key must be %NULL, else the return value will always be 0.
 */
int hashmap_put(hashmap *h, void *key, void *value)
{
    if (h != NULL && key != NULL) {
        entry_t *entry;
        unsigned int hash = h->hash(key);
        int rc;

        if ((entry = hashmap_find_entry(h, key, hash))) {
            if (entry->key != key && entry->key != value) {
                if (h->key_destructor) {
                    h->key_destructor(entry->key);
                }
                if (entry->key == entry->value) {
                    entry->value = NULL;
                }
            }
            if (entry->value != value && entry->value != key) {
                if (h->value_destructor) {
                    h->value_destructor(entry->value);
                }
            }

            entry->key = (void *)key;
            entry->value = (void *)value;

            return 1;
        }

        // a new entry
        if ((rc = hashmap_new_entry(h, hash, key, value)) < 0) {
            return rc;
        }
    }

    return 0;
}

/**
 * \param[in] h Hashmap
 * \param[in] key Key to search for
 * \return 1 if \p key has been found, 0 if not found
 *
 * Check if \p key exists in \p h.
 */
int hashmap_contains(const hashmap *h, const void *key)
{
    return hashmap_get(h, key, NULL);
}

/**
 * \param[in] h Hashmap
 * \param[in] key Key to search for
 * \param[out] value Value to be returned
 * \return 1 if \p key has been found, 0 if not found
 *
 * Get the value for a given key.
 *
 * Neither \p h nor \p key must be %NULL.
 */
#undef hashmap_get
int hashmap_get(const hashmap *h, const void *key, void **value)
{
    if (h && key) {
        entry_t *entry;

        if ((entry = hashmap_find_entry(h, key, h->hash(key)))) {
            if (value) {
                *value = entry->value;
            }
            return 1;
        }
    }

    return 0;
}

static int hashmap_remove_entry(hashmap *h, const char *key, int free_kv)
{
    entry_t *entry, *next, *prev = NULL;
    unsigned int hash = h->hash(key);
    int pos = (int)(hash % (unsigned int)h->max);

    for (entry = h->entry[pos]; entry; prev = entry, entry = next) {
        next = entry->next;

        if (hash == entry->hash && (key == entry->key || !h->cmp(key, entry->key))) {
            if (prev) {
                prev->next = next;
            } else {
                h->entry[pos] = next;
            }

            if (free_kv) {
                if (h->key_destructor) {
                    h->key_destructor(entry->key);
                }
                if (entry->value != entry->key) {
                    if (h->value_destructor) {
                        h->value_destructor(entry->value);
                    }
                }
                entry->key = NULL;
                entry->value = NULL;
            }
            warc_free(entry);

            h->cur--;
            return 1;
        }
    }

    return 0;
}

/**
 * \param[in] h Hashmap
 * \param[in] key Key to be removed
 * \return 1 if \p key has been removed, 0 if not found
 *
 * Remove \p key from hashmap \p h.
 *
 * If \p key is found, the key and value destructor functions are called
 * when removing the entry from the hashmap.
 */
int hashmap_remove(hashmap *h, const void *key)
{
    if (h && key) {
        return hashmap_remove_entry(h, key, 1);
    } else {
        return 0;
    }
}

/**
 * \param[in] h Hashmap
 * \param[in] key Key to be removed
 * \return 1 if \p key has been removed, 0 if not found
 *
 * Remove \p key from hashmap \p h.
 *
 * Key and value destructor functions are *not* called when removing the entry from the hashmap.
 */
int hashmap_remove_nofree(hashmap *h, const void *key)
{
    if (h && key) {
        return hashmap_remove_entry(h, key, 0);
    } else {
        return 0;
    }
}

/**
 * \param[in] h Hashmap to be free'd
 *
 * Remove all entries from hashmap \p h and free the hashmap instance.
 *
 * Key and value destructor functions are called for each entry in the hashmap.
 */
void hashmap_free(hashmap **h)
{
    if (h && *h) {
        hashmap *temp;
        temp = *h;
        hashmap_clear(*h);
        warc_free(temp->entry);
        warc_free(*h);
    }
}

/**
 * \param[in] h Hashmap to be cleared
 *
 * Remove all entries from hashmap \p h.
 *
 * Key and value destructor functions are called for each entry in the hashmap.
 */
void hashmap_clear(hashmap *h)
{
    if (h) {
        entry_t *entry, *next;
        int it, cur = h->cur;

        for (it = 0; it < h->max && cur; it++) {
            for (entry = h->entry[it]; entry; entry = next) {
                next = entry->next;

                if (h->key_destructor) {
                    h->key_destructor(entry->key);
                }

                // free value if different from key
                if (entry->value != entry->key && h->value_destructor) {
                    h->value_destructor(entry->value);
                }

                entry->key = NULL;
                entry->value = NULL;

                warc_free(entry);
                cur--;
            }
            h->entry[it] = NULL;
        }
        h->cur = 0;
    }
}

/**
 * \param[in] h Hashmap
 * \return Number of entries in hashmap \p h
 *
 * Return the number of entries in the hashmap \p h.
 */
int hashmap_size(const hashmap *h)
{
    return h ? h->cur : 0;
}

/**
 * \param[in] h Hashmap
 * \param[in] browse Function to be called for each element of \p h
 * \param[in] ctx Context variable use as param to \p browse
 * \return Return value of the last call to \p browse
 *
 * Call function \p browse for each element of hashmap \p h or until \p browse
 * returns a value not equal to zero.
 *
 * \p browse is called with \p ctx and the pointer to the current element.
 *
 * The return value of the last call to \p browse is returned or 0 if either \p h or \p browse is %NULL.
 */
int hashmap_browse(const hashmap *h, hashmap_browse_fn *browse, void *ctx)
{
    if (h && browse) {
        entry_t *entry;
        int it, ret, cur = h->cur;

        for (it = 0; it < h->max && cur; it++) {
            for (entry = h->entry[it]; entry; entry = entry->next) {
                if ((ret = browse(ctx, entry->key, entry->value)) != 0) {
                    return ret;
                }
                cur--;
            }
        }
    }

    return 0;
}

/**
 * \param[in] h Hashmap
 * \param[in] cmp Comparison function used to find keys
 *
 * Set the comparison function.
 */
void hashmap_setcmpfunc(hashmap *h, hashmap_compare_fn *cmp)
{
    if (h) {
        h->cmp = cmp;
    }
}

/**
 * \param[in] h Hashmap
 * \param[in] hash Hash function used to hash keys
 * \return 0 if set successfully, else -1
 *
 * Set the key hash function.
 *
 * The keys of all entries in the hashmap will be hashed again. This includes a memory allocation, so
 * there is a possibility of failure.
 */
int hashmap_sethashfunc(hashmap *h, hashmap_hash_fn *hash)
{
    if (!h) {
        return -1;
    }

    if (!h->cur) {
        return 0; // no re-hashing needed
    }

    entry_t **new_entry = calloc((size_t)h->max, sizeof(entry_t *));

    if (!new_entry) {
        return -1;
    }

    h->hash = hash;
    hashmap_rehash(h, new_entry, h->max, 1);

    return 0;
}

/**
 * \param[in] h Hashmap
 * \param[in] destructor Destructor function for keys
 *
 * Set the key destructor function.
 *
 * Default is warc_free().
 */
void hashmap_set_key_destructor(hashmap *h, hashmap_key_destructor *destructor)
{
    if (h) {
        h->key_destructor = destructor;
    }
}

/**
 * \param[in] h Hashmap
 * \param[in] destructor Destructor function for values
 *
 * Set the value destructor function.
 *
 * Default is warc_free().
 */
void hashmap_set_value_destructor(hashmap *h, hashmap_value_destructor *destructor)
{
    if (h) {
        h->value_destructor = destructor;
    }
}

/**
 * \param[in] h Hashmap
 * \param[in] factor The load factor
 *
 * Set the load factor function.
 *
 * The load factor is determines when to resize the internal memory.
 * 0.75 means "resize if 75% or more of all slots are used".
 *
 * The resize strategy is set by hashmap_set_growth_policy().
 *
 * The resize (and rehashing) occurs earliest on the next insertion of a new key.
 *
 * Default is 0.75.
 */
void hashmap_set_load_factor(hashmap *h, float factor)
{
    if (h) {
        h->load_factor = factor;
        h->threshold = (int)((float)h->max * h->load_factor);
        // rehashing occurs earliest on next put()
    }
}

/**
 * \param[in] h Hashmap
 * \param[in] factor Hashmap growth factor
 *
 * Set the factor for resizing the hashmap when it's load factor is reached.
 *
 * The new size is 'factor * oldsize'. If the new size is less or equal 0,
 * the involved put function will do nothing and the internal state of
 * the hashmap will not change.
 *
 * Default is 2.
 */
void hashmap_set_resize_factor(hashmap *h, float factor)
{
    if (h) {
        h->resize_factor = factor;
    }
}
