# warc_manywarnings.m4 serial 1
dnl Copyright (C) 2016-2020 Free Software Foundation, Inc.
dnl This file is free software; the Free Software Foundation
dnl gives unlimited permission to copy and/or distribute it,
dnl with or without modifications, as long as this notice is preserved.

dnl Originally from wget_manywarnings.m4 by Tim Ruehsen.
dnl LibWARC version maintained by Darshit Shah

# warc_WORD_SET(RESULT, SET, WORDS)
# --------------------------------------------------
# Add each word in WORDS to SET if not already there and store in RESULT.
# Words separated by whitespace.
AC_DEFUN([warc_WORD_SET], [
  ret=$2
  words=" $2 "
  for word in $3; do
    if test "${words#*" $word "}" = "$words"; then ret="$ret $word"; fi
  done
  $1=$ret
])

# warc_WORD_REMOVE(RESULT, SET, WORDS)
# --------------------------------------------------
# Remove each word in WORDS from SET and store in RESULT.
# Words separated by whitespace.
AC_DEFUN([warc_WORD_REMOVE], [
  ret=
  words=" $3 "
  for word in $2; do
     if test "${words#*" $word "}" = "$words"; then ret="$ret $word"; fi
  done
  $1=$ret
])

# warc_MANYWARNINGS_INIT(VARIABLE, LANGUAGE)
# -----------------------------
# Add LANGUAGE related GCC warnings to VARIABLE.
# This only works for gcc >= 4.3.
AC_DEFUN([warc_MANYWARNINGS_INIT], [

  $1=""

  if test x"$enable_developer_mode" = xyes; then
    # AC_PROG_CC sets $GCC to 'yes' if compiler is gcc
    # AC_REQUIRE([AC_PROG_CC])

    case $CC in
      *gcc*) CCNAME="gcc";;
      *clang*) CCNAME="clang";;
    esac

    if test "$CCNAME" = "gcc"; then
      test -n "$2" && warc_LANGUAGE=$2 || warc_LANGUAGE=C

      # add -Wall -Wextra to reduce number of warn flags
      warc_WORD_SET([warc_WARN_CFLAGS], [$CFLAGS], ["-Wall -Wextra -Wformat=2"])

      # collect all disabled warn flags in $WARN_CFLAGS
      # remove -Werror-implicit-function-declaration (see https://gcc.gnu.org/bugzilla/show_bug.cgi?id=90690)
      warc_WARN_CFLAGS=$warc_WARN_CFLAGS" "$($CC $warc_WARN_CFLAGS -Q --help=warning,$warc_LANGUAGE|\
        awk '{ if (([$]2 == "[[disabled]]" || [$]2 == "")\
          && [$]1!~/=/ && [$]1~/^-W/ && [$]1!~/</ && [$]1!="-Wall" && [$]1!~/^-Werror-/) print [$]1 }')

      GCC_VERSION=$($CC -dumpversion | cut -f1 -d.)
      if test $GCC_VERSION -ge 6; then
        warc_WARN_CFLAGS=$warc_WARN_CFLAGS" -Warray-bounds=2 -Wnormalized=nfc -Wshift-overflow=2 -Wunused-const-variable=2"
      fi
      if test $GCC_VERSION -ge 7; then
        warc_WARN_CFLAGS=$warc_WARN_CFLAGS" -Wformat-overflow=2 -Wformat-truncation=1 -Wstringop-overflow=2"
      fi

    elif test "$CCNAME" = "clang"; then
      # set all warn flags on
      warc_WORD_SET([warc_WARN_CFLAGS], [$CFLAGS], ["-Weverything"])
    fi

    $1=$warc_WARN_CFLAGS
  fi
])

# warc_MANYWARNINGS_CLEAN(VARIABLE)
# -----------------------
# Clean the VARIABLE. Remove warning flags that are not desired.
# Add warning flags that are not usually there, but desired
AC_DEFUN([warc_MANYWARNINGS_CLEAN], [
    AC_REQUIRE([warc_MANYWARNINGS_INIT])

    if test "$CCNAME" = "gcc"; then
      # Set up list of unwanted warnings
      nw=
      nw="$nw -Wsystem-headers"       # System headers may trigger lot's of useless warnings
      nw="$nw -Wtraditional"
      nw="$nw -Wtraditional-conversion"
      nw="$nw -Wc++-compat"
      nw="$nw -Wabi"
      nw="$nw -Wdeclaration-after-statement" # C89 only, messing up gcc < 5
      nw="$nw -Wchkp" # gcc-9 says: not supported any more (but emits it via -Q --help=C)"
      if test "$cross_compiling" = yes; then
        WARN_CFLAGS="$WARN_CFLAGS -Wno-format"
        nw="$nw -Wformat"
      fi

      # remove unwanted warn flags
      warc_WORD_REMOVE([WARN_CFLAGS], [$WARN_CFLAGS], [$nw])

      # add more flags as you like
      if test $GCC_VERSION -ge 5; then
        WARN_CFLAGS="$WARN_CFLAGS -fdiagnostics-color=always"
	WARN_CFLAGS="$WARN_CFLAGS -fipa-pure-const -Wsuggest-attribute=pure"
	WARN_CFLAGS="$WARN_CFLAGS -Wsuggest-attribute=const -Wsuggest-attribute=noreturn"
      fi
    fi
    WARN_CFLAGS=`echo $WARN_CFLAGS | tr '\n' ' '`
])

# warc_MANYWARNINGS_GL(DESTVARIABLE, SRCVARIABLE)
# --------------------
# Set the variable DESTVARIABLE for warnings flags that should be passed
# to gnulib. This is a smaller set of warnings as compared to the ones
# used when compiling others
# Removing is not enough if these switches are implicitly set by other
# flags like -Wall or -Wextra. We have to explicitly unset them
# with -Wno-....
AC_DEFUN([warc_MANYWARNINGS_GL], [
    AC_REQUIRE([warc_MANYWARNINGS_INIT])

    WARN_FLAGS_gl=""

    nw=
    nw="$nw -Wpedantic"
    nw="$nw -Wsign-compare"
    nw="$nw -Wunused-parameter"
    nw="$nw -Wswitch-default"
    nw="$nw -Wformat-nonliteral"
    nw="$nw -Wsuggest-attribute=pure"
    nw="$nw -Wsuggest-attribute=const"
    nw="$nw -Wsuggest-attribute=noreturn"
    nw="$nw -Wunsafe-loop-optimizations"
    nw="$nw -Wunused-macros"
    nw="$nw -Wundef"
    nw="$nw -Wswitch-enum"
    nw="$nw -Wcast-qual"
    nw="$nw -Wbad-function-cast"
    nw="$nw -Wredundant-decls"
    nw="$nw -Werror"
    warc_WORD_REMOVE([WARN_FLAGS_gl], [$WARN_CFLAGS], [$nw])

    # disable options implicitly set by other options
    WARN_FLAGS_gl="$WARN_FLAGS_gl -Wno-sign-compare -Wno-unused-parameter -Wno-alloca"
    WARN_FLAGS_gl="$WARN_FLAGS_gl -Wno-float-conversion -Wno-cast-function-type"
    WARN_FLAGS_gl="$WARN_FLAGS_gl -Wno-sign-conversion -Wno-conversion"
    WARN_FLAGS_gl="$WARN_FLAGS_gl -Wno-format-nonliteral"
    if test "$cross_compiling" = yes; then
      WARN_FLAGS_gl="$WARN_FLAGS_gl -Wno-incompatible-pointer-types"
    fi

    $1="$WARN_FLAGS_gl"
])
